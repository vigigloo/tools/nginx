# nginx

A generic helm chart, for nginx running on vigigloo

## Configuring CronJobs

This chart allows configuring cronjobs with the following format:
```yaml
jobs:
  cron:
    ## Required
    # The cron schedule expression
    frequency: ""
    # Suffix for the CronJob name
    suffix: ""

    ## Optional
    # Arguments for the CronJob as a list of strings
    args: []
```

## Configuring deploy hooks

This chart allows configuring deploy hooks with the following format:
```yaml
jobs:
  hooks:
    postDelete:
      ## Required
      # Suffix for the job name
      suffix: ""

      ## Optional
      image:
        # Repository to use to fetch the image that will run the job
        # Defaults to image.repository
        repository: ""
        # Image tag for this job
        # Defaults to image.tag
        tag: ""
      # Command to run for the job as a list of strings
      command: []
      # Arguments for the command as a list of strings
      args: []

    ## Other available hooks
    postInstall: {}
    preInstall: {}
    preUpgrade: {}
```
